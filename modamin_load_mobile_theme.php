<?php
/**
 * @package Modamin load mobile theme
 * @version 1.05
 */
/*
Plugin Name: Modamin Load Mobile Theme
Plugin URI: http://ressan.ir/
Description: This plugin loads mobile theme where needed instead of default theme.
Author: Nasser Mansouri
Version: 1.01
Author URI: http://ressan.ir/
*/




function modamin_laod_mobile_template($theme) {
	$_get_query_key = 'use_mobile_theme';
	$_cookie_name = 'modamin_load_mobile_theme';
	$_mobile_theme_name = get_option( 'modamin_load_mobile_theme_theme_name' );

	$_use_mobile_theme = $_GET[$_get_query_key];
	if(isset($_use_mobile_theme) and ($_use_mobile_theme === 'true')){
		setcookie($_cookie_name, 'true', time() + (60*60*24*30), "/");
		return $_mobile_theme_name;
	} else if(isset($_use_mobile_theme) and ($_use_mobile_theme === 'false')){
		setcookie($_cookie_name, 'false', time() + (60*60*24*30), "/");
		return $theme;
	} else {
		$_cookie = $_COOKIE[$_cookie_name];
		if(isset($_cookie) and ($_cookie === 'true')){
			return $_mobile_theme_name;
		} else {
			return $theme;
		}
	}
}

function modamin_laod_mobile_stylesheet($theme) {
	$_get_query_key = 'use_mobile_theme';
	$_cookie_name = 'modamin_load_mobile_theme';
	$_mobile_theme_name = get_option( 'modamin_load_mobile_theme_theme_name' );

	$_use_mobile_theme = $_GET[$_get_query_key];
	if(isset($_use_mobile_theme) and ($_use_mobile_theme === 'true')){
		setcookie($_cookie_name, 'true', time() + (60*60*24*30), "/");
		return $_mobile_theme_name;
	} else if(isset($_use_mobile_theme) and ($_use_mobile_theme === 'false')){
		setcookie($_cookie_name, 'false', time() + (60*60*24*30), "/");
		return $theme;
	} else {
		$_cookie = $_COOKIE[$_cookie_name];
		if(isset($_cookie) and ($_cookie === 'true')){
			return $_mobile_theme_name;
		} else {
			return $theme;
		}
	}
}

add_filter( 'template', 'modamin_laod_mobile_template' );
add_filter( 'stylesheet', 'modamin_laod_mobile_stylesheet' );





add_action('admin_menu' , 'modamin_load_mobile_theme_plugin_menu');
function modamin_load_mobile_theme_plugin_menu(){
	add_menu_page( 'Select alternative theme for mobile devices',
		'Select mobile theme',
		'manage_options',
		'modamin-load-mobile-theme-select-theme',
		'modamin_load_mobile_theme_select_theme' );
}

function modamin_load_mobile_theme_select_theme(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	$opt_name = 'modamin_load_mobile_theme_theme_name';
    $hidden_field_name = 'mt_submit_hidden';
    $data_field_name = 'mt_favorite_color';
	$opt_val = get_option( $opt_name );

	if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
        $opt_val = $_POST[ $data_field_name ];
        update_option( $opt_name, $opt_val );
		?>
		<div class="updated"><p><strong><?php _e('saved.', 'menu-test' ); ?></strong></p></div>
		<?php
	}
    ?>
	<div class="wrap">
		<h2> <?php echo __( 'Select alternative theme for mobile devices', 'menu-test' ) ?> </h2>
		<form name="form1" method="post" action="">
			<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
			<p><?php _e("Name of alternative theme", 'menu-test' ); ?>
				<?php $installed_themes = wp_get_themes();?>
				<select name="<?php echo $data_field_name; ?>">
					<option value=""><?php _e("select", 'menu-test' ); ?></option>
					<?php foreach($installed_themes as $wp_th) {?>
					<option value="<?php echo $wp_th->get_stylesheet() ?>"
						<?php echo ($wp_th->get_stylesheet() === $opt_val) ? "selected":""; ?> >
							<?php echo $wp_th->__get('name'); ?>
					</option>
					<?php } ?>
				</select>
			</p>
			<hr />
			<p class="submit">
				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save') ?>" />
			</p>
		</form>
	</div>

<?php
}






function register_modamin_mobile_menu() {
  register_nav_menu('modamin-mobile-menu',__( 'Modamin Mobile Menu' ));
}
add_action( 'init', 'register_modamin_mobile_menu' );


?>
